import { localize } from "ember-simple-i18n";

export default Ember.Handlebars.makeBoundHelper(localize);